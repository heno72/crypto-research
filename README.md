# crypto-research

A research journal I've done done on crypto.

## Description

The name of this repository stands as is: ```crypto-research```.
Though keep in mind that I am merely a legal jurist, not a computer scientist.
Most researches I did that are codified here is not the most technical research, nor can I make one.

I made this repository mainly to serve myself, as a way to document what I have learned about crypto in general.
Probably not all that I've learned is stored here, but I will try to at least include important topics.

## Contents

| No. | Title | Description |
| :-- | :---- | :---------- |
| 001 | [Smart contract addresses](docs/smart-contract-addresses.md) | A list of some smart contract addresses on popular blockchains |
| 002 | [Add Custom Networks On Metamask](docs/metamask-add-network.md) | A short guide on how to add custom networks on metamask |
| 003 | [Use Unstoppable Domains](docs/use-unstoppable-domains.md) | A step by step guide to browse unstoppable domains |
| 004 | [Deterministic vs. Nondeterministic Wallets](docs/deterministic-vs-nondeterministic-wallets.md) | A brief review of various wallet technologies |
| 005 | [Address Reuse](docs/address-reuse.md) | To briefly explain various transaction models on crypto |
| 006 | [DApp, Web3 and connecting a wallet](docs/dapp-web3-and-connecting-a-wallet.md) | A short guide on connecting our wallets to DApps |
| 007 | [NFT on Opensea.io](docs/nft-on-opensea.md) | Understanding how to make and sell NFT in opensea.io |
| 008 | [BIP85 Use Case](docs/bip85.md) | Interesting use case of BIP85 and its implication on backing up HD wallets with seed phrases. |
| 009 | [ArDrive Public Drives](docs/ardrive-public-drives.md) | The ArDrive community has created hundreds of Public Drives that are available to explore forever. I also add some of my own. |
| 010 | [ArDrive Permaweb Link](docs/ardrive-permaweb-link.md) | On how to use ArDrive-based links for permanent data storage (including NFT) |
| 011 | [Crypto Network RPC](docs/network-rpc.md) | A curated list of RPC |
| 101 | [Crypto Investment Plan](docs/investment-plan-a.md) | A quick guide on how to plan crypto investment |

