# ArDrive Public Drives

In this page I collect various public drives I could find in ArDrive. There are community public drives, and there are also some that I personally own.

## Personal ArDrive Public Drive

- [hendriklie72-works](https://app.ardrive.io/#/drives/51215bb7-c031-4321-a6c5-12f493d7d278?name=hendriklie72-works)
  
  Drive ID: 51215bb7-c031-4321-a6c5-12f493d7d278

- [Photograph Collections](https://app.ardrive.io/#/drives/851a0382-795c-4599-8141-7628c2b45ec4?name=Photograph+Collections)

  Drive ID: 851a0382-795c-4599-8141-7628c2b45ec4


## ArDrive Community's

This is a compilation of [this atlassian page](https://ardrive.atlassian.net/wiki/spaces/help/pages/86835201/Exploring) and [ardrive's own feature blog](https://ardrive.io/features/public-or-private/).

The ArDrive community has created hundreds of Public Drives that are available to explore forever.  The following are a few examples of Public Drives that have been created:

- [Ros McMahon’s Movie Collection](https://app.ardrive.io/#/drives/12705673-0486-46f5-b07e-49d0bf68178c?name=Public+Domain+%28Copyright-free%29+Movies) - a carefully selected compilation of copyright expired movies.
  
  Drive ID: 12705673-0486-46f5-b07e-49d0bf68178c

- [ArDrive Official Public Drive](https://app.ardrive.io/#/drives/bfe2ff28-6efe-4a00-8d1e-ad5f091e28a6?name=ArDrive+Official+Public) - pictures, videos, presentations and other permanent content from the ArDrive community.

  Drive ID: bfe2ff28-6efe-4a00-8d1e-ad5f091e28a6

- [51 Books in the Public Domain](https://app.ardrive.io/#/drives/e98a8ad5-2f4e-41e9-b581-35cf06777a16?name=Books) - curated by drkiddo

  Drive ID: e98a8ad5-2f4e-41e9-b581-35cf06777a16

- [A Bite of China - Wonderful Chinese Food](https://app.ardrive.io/#/drives/80d94d51-558a-4d7b-8cd7-4db7881a2e27?name=A+Bite+of+China-Wonderful+Chinese+Food) - recipes, restaurant guides, food maps and more

  Drive ID: 80d94d51-558a-4d7b-8cd7-4db7881a2e27

- [Apple Daily](https://app.ardrive.io/#/drives/8102fcac-6da8-419d-9c6a-1a28b6c741f9/folders/dc7d6734-bc81-4d75-b8cf-92d754dd4acf) - the last issue of the Hong Kong pro-democracy newspaper before it was taken offline.

  Drive ID: 8102fcac-6da8-419d-9c6a-1a28b6c741f9

- [Arweave Puzzles](https://app.ardrive.io/#/drives/953f7e9e-c4f6-4cbc-8699-f07d89518a90?name=Arweave+Puzzles) - a collection of Arweave-themed puzzles from @ArweaveP

  Drive ID: 953f7e9e-c4f6-4cbc-8699-f07d89518a90

- [Cairo, my hometown](https://app.ardrive.io/#/drives/70776236-8247-4d52-8a5a-bfb71d5702b9?name=My+Hometown+--+Nanhui) - maps, photos and local food guides for Cairo, Egypt

  Drive ID: af5d8907-e761-4c87-9158-2def579a9784

- [Electronica Music](https://app.ardrive.io/#/drives/42b78804-79f0-4a4d-90d9-004b97ff2eca?name=NFT%20Studio%20Assets%20-%20Permanent%20Collection) - “dim, neon-lit, smoky club at 3:30am in some dystopian future" 

  Drive ID:  42b78804-79f0-4a4d-90d9-004b97ff2eca

- [Gender and Migration Hub](https://app.ardrive.io/#/drives/8e303abf-74c9-47c7-8997-8025ceb3f210?name=Gender+and+Migration+Hub) - academic resources on global migration policy 

  Drive ID: 8e303abf-74c9-47c7-8997-8025ceb3f210

- [Historic Mechanical Keyboards](https://app.ardrive.io/#/drives/8e303abf-74c9-47c7-8997-8025ceb3f210?name=Gender+and+Migration+Hub) - curated collection of technical manuals for early keyboards

  Drive ID: e5939778-aea1-43ea-8bff-662b7e7c3925

- [Nannui, Shanghai](https://app.ardrive.io/#/drives/70776236-8247-4d52-8a5a-bfb71d5702b9?name=My+Hometown+--+Nanhui) - photos, videos and historical information about this neighbourhood

  Drive ID: 70776236-8247-4d52-8a5a-bfb71d5702b9

- [Regent College](https://app.ardrive.io/#/drives/94dc341b-ef62-4eb2-b98d-571a8054aeff?name=2010-2020+Regent+Audio) - lectures on theology and culture

  Drive ID: 94dc341b-ef62-4eb2-b98d-571a8054aeff

- [Ros McMahon’s Movie Collection](https://app.ardrive.io/#/drives/12705673-0486-46f5-b07e-49d0bf68178c?name=Public+Domain+%28Copyright-free%29+Movies) - a carefully selected compilation of copyright expired movies 

  Drive ID: 12705673-0486-46f5-b07e-49d0bf68178c

- [They Made Seven Thomas Jeffersons](https://app.ardrive.io/#/drives/38470fce-8da1-4a20-a1be-d0be38776aab) - a short story from multimedia artist Neil Von Flue

  Drive ID: 38470fce-8da1-4a20-a1be-d0be38776aab

