# Smart Contract Addresses

## BEP20

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| BTCB  | `0x7130d2a12b9bcbfae4f2634d864a1ee1ce3ead9c` | 18 |
| ETH   | `0x2170Ed0880ac9A755fd29B2688956BD959F933F8` | 18 |
| BUSD  | `0xe9e7cea3dedca5984780bafc599bd69add087d56` | 18 |
| USDC  | `0x8ac76a51cc950d9822d68b83fe1ad97b32cd580d` | 18 |
| FRAX  | `0x90C97F71E18723b0Cf0dfa30ee176Ab653E89F40` | 18 |
| USDT  | `0x55d398326f99059ff775485246999027b3197955` | 18 |
| VAI   | `0x4bd17003473389a42daf6a0a729f6fdb328bbbd7` | 18 |
| FXS   | `0xe48A3d7d0Bc88d552f730B62c006bC925eadB9eE` | 18 |
| XVS   | `0xcf6bb5389c92bdda8a3747ddb454cb7a64626c63` | 18 |
| VBTC  | `0x882c173bc7ff3b7786ca16dfed3dfffb9ee7847b` | 8 |

Source: [BscScan](https://bscscan.com/)

## ETH20

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| WETH  | `0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2` | 18 |
| WBTC  | `0x2260FAC5E5542a773Aa44fBCfeDf7C193bc2C599` | 8 |
| USDC  | `0xa0b86991c6218b36c1d19d4a2e9eb0ce3606eb48` | 6 |
| BUSD  | `0x4fabb145d64652a948d72533023f6e7a623c7c53` | 18 |
| FRAX  | `0x853d955acef822db058eb8505911ed77f175b99e` | 18 |
| DAI   | `0x6B175474E89094C44Da98b954EedeAC495271d0F` | 18 |
| aUSDC | `0xBcca60bB61934080951369a648Fb03DF4F96263C` | 6 |
| MKR   | `0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2` | 18 |
| FXS   | `0x3432B6A60D23Ca0dFCa7761B7ab56459D9C964D0` | 18 |
| USDT  | `0xdac17f958d2ee523a2206206994597c13d831ec7` | 6 |
| USDP  | `0x8E870D67F660D95d5be530380D0eC0bd388289E1` | 18 |
| PAXG  | `0x45804880De22913dAFE09f4980848ECE6EcbAf78` | 18 |

Source: [Etherscan](https://etherscan.io/) for most coins, [Aave AToken Docs](https://docs.aave.com/developers/tokens/atoken) and [Aave Docs for Aave v2 token](https://docs.aave.com/developers/v/2.0/deployed-contracts/deployed-contracts).

## Polygon

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| WETH  | `0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619` | 18 |
| WBTC  | `0x1BFD67037B42Cf73acF2047067bd4F2C47D9BfD6` | 8 |
| USDC  | `0x2791Bca1f2de4661ED88A30C99A7a9449Aa84174` | 6 |
| FRAX  | `0x45c32fA6DF82ead1e2EF74d17b76547EDdFaFF89` | 18 |
| DAI   | `0x8f3Cf7ad23Cd3CaDbD9735AFf958023239c6A063` | 18 |
| aPolUSDC | `0x625E7708f30cA75bfd92586e17077590C60eb4cD` | 6 |
| aPolDAI | `0x82E64f49Ed5EC1bC6e43DAD4FC8Af9bb3A2312EE` | 18 |
| aPolWMATIC | `0x6d80113e533a2C0fe82EaBD35f1875DcEA89Ea97` | 18 |
| aPolWETH | `0xe50fA9b3c56FfB159cB0FCA61F5c9D750e8128c8` | 18 |
| aPolWBTC | `0x078f358208685046a11C85e8ad32895DED33A249` | 8 |
| BUSD  | `0xdAb529f40E671A1D4bF91361c21bf9f0C9712ab7` | 18 |
| FXS   | `0x1a3acf6D19267E2d3e7f898f42803e90C9219062` | 18 |
| PAXG  | `0x553d3D295e0f695B9228246232eDF400ed3560B5` | 18 |
| USDT  | `0xc2132D05D31c914a87C6611C10748AEb04B58e8F` | 6 |

Source: [Polygon Scan](https://polygonscan.com/) for most coins, [Aave AToken Docs](https://docs.aave.com/developers/tokens/atoken) and [Aave Docs for Aave V3 contracts](https://docs.aave.com/developers/deployed-contracts/v3-mainnet/polygon).

## Arbitrum

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| WBTC  | `0x2f2a2543B76A4166549F7aaB2e75Bef0aefC5B0f` | 8 |
| WETH  | `0x82aF49447D8a07e3bd95BD0d56f35241523fBab1` | 18 |
| USDC  | `0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8` | 6 |
| FRAX  | `0x17FC002b466eEc40DaE837Fc4bE5c67993ddBd6F` | 18 |
| FXS   | `0x9d2F299715D94d8A7E6F5eaa8E654E8c74a988A7` | 18 |
| USDT  | `0xFd086bC7CD5C481DCC9C85ebE478A1C0b69FCbb9` | 6 |

Source: [Arbiscan](https://arbiscan.io/)

## Optimism

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| WETH  | `0x4200000000000000000000000000000000000006` | 18 |
| WBTC  | `0x68f180fcCe6836688e9084f035309E29Bf0A2095` | 8 |
| USDC  | `0x7F5c764cBc14f9669B88837ca1490cCa17c31607` | 6 |
| DAI  | `0xDA10009cBd5D07dd0CeCc66161FC93D7c9000da1` | 18 |
| FRAX  | `0x2E3D870790dC77A83DD1d18184Acc7439A53f475` | 18 |
| FXS  | `0x67CCEA5bb16181E7b4109c9c2143c24a1c2205Be` | 18 |

Source: [Optimistic Etherscan](https://optimistic.etherscan.io/)

## Gnosis Chain

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| WETH  | `0x6A023CCd1ff6F2045C3309768eAd9E68F978f6e1` | 18 |
| WBTC  | `0x8e5bBbb09Ed1ebdE8674Cda39A0c169401db4252` | 8 |
| USDC  | `0xDDAfbb505ad214D7b80b1f830fcCc89B60fb7A83` | 6 |
| DAI  | `0x44fA8E6f47987339850636F88629646662444217` | 18 |

Source: [Blockscout](https://blockscout.com/xdai/mainnet/)

## TRX20

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| USDT  | `TR7NHqjeKQxGTCi8q8ZY4pL8otSzgjLj6t` | 6 |
| USDC  | `TEkxiTehnzSmSe2XqrBj4w32RUN966rdz8` | 6 |
| USDD | `TPYmHEhy5n8TCEfYGqW2rPxsghSfzghPDn` | 18 |

Source: [Tronscan](https://tronscan.org/)

## Avalanche (C-Chain)

| Token | Contract Addresses | Decimals |
| :---- | :----------------- | -------: |
| BUSD.e | `0x19860CCB0A68fd4213aB9D8266F7bBf05A8dDe98` | 18 |
| USDT.e | `0xc7198437980c041c805A1EDcbA50c1Ce5db95118` | 6 |
| USDC.e | `0xA7D7079b0FEaD91F3e65f86E8915Cb59c1a4C664` | 6 |
| WETH.e | `0x49D5c2BdFfac6CE2BFdB6640F4F80f226bc10bAB` | 18 |
| WBTC.e | `0x50b7545627a5162F82A992c33b87aDc75187B218` | 8 |
| DAI.e | `0xd586E7F844cEa2F87f50152665BCbc2C279D8d70` | 18 |
| USDC[^usdcavax] | `0xB97EF9Ef8734C71904D8002F8b6Bc66Dd9c48a6E` | 6 |
| USDt[^usdtavax] | `0x9702230A8Ea53601f5cD2dc00fDBc13d4dF4A8c7` | 6 |

Source: [Snowtrace](https://snowtrace.io/)

[^usdcavax]: Apparently the `.e` version is the latest and preferred standard for avax bridged tokens.
[^usdtavax]: *Ibid.*

## Terra

Because of Terra's failure to maintain peg, I have to remove them from other parts so not to clutter them.
I will have them down here for recordkeeping instead.
There is no use of keeping them, as they have no use case at the moment.

I will split them into native and extrachains.

### Native

| Token | Type  | Contract Addresses | Decimals |
| :---- | :---- | :----------------- | -------: |
| UST[^ustaddr] | Coins | `uusd` | 6 |
| aUST | CW20 | `terra1hzh9vpxhsk8253se0vv5jj6etdvxu3nv8z07zu` | 6 |
| vUST | CW20 | `terra1w0p5zre38ecdy3ez8efd5h9fvgum5s206xknrg` | 6 |
| bLUNA | CW20 | `terra1kc87mu460fwkqte29rquh4hc20m54fxwtsx7gp` | 6 |
| bETH | CW20 | `terra1dzhzukyezv0etz22ud940z7adyv7xgcjkahuun` | 6 |
| WETH | CW20 | `terra1u5szg038ur9kzuular3cae8hq6q5rk5u27tuvz` | 8 |
| stETH | CW20 | `terra1w7ywr6waxtjuvn5svk5wqydqpjj0q9ps7qct4d` | 8 |

Source: [Terrascope](https://terrasco.pe/)

[^ustaddr]: It's surprisingly hard to find. Even then I don't know its decimals, I have to manually count it on my wallet.

### Extrachains

| Chain | Token | Contract Addresses | Decimals |
| :---- | :---- | :----------------- | -------: |
| BEP20 | USTw[^wormholeustbsc] | `0x3d4350cD54aeF9f9b2C29435e0fa809957B3F30a` | 6 |
| BEP20 | UST[^wrappedustbsc] | `0x23396cF899Ca06c4472205fC903bDB4de249D6fC` | 18 |
| Polygon | UST[^shuttlewrappedpolygon] | `0x692597b009d13C4049a947CAB2239b7d6517875F` | 18 |
| Polygon | UST[^wormholeustpolygon] | `0xE6469Ba6D2fD6130788E0eA9C0a0515900563b59` |
| Arbitrum | UST | `0x13780E6d5696DD91454F6d3BbC2616687fEa43d0` | 6 |

Source: [Arbiscan](https://arbiscan.io/), [BscScan](https://bscscan.com/), [Polygon Scan](https://polygonscan.com/)

[^shuttlewrappedpolygon]: This is a Shuttle-wrapped asset from Polygon! Weirdly this is what we get to withdraw UST on Polygon.
[^wormholeustpolygon]: This is a Wormhole UST on Polygon. It is what we get by transferring UST from Terra to Polygon via wormhole.
[^wormholeustbsc]: This address is obtained from [Terra Bridge](https://bridge.terra.money)
[^wrappedustbsc]: This address is from bscscan, under the name of Wrapped UST Token.

