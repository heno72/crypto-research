Source: https://rpc.info/

# Avalanche RPCs

**Type:** Mainnet
**Chain ID:** 43114
**Symbol:** AVAX
**Block explorer:** https://snowtrace.io/

**avax.network RPC:** `https://api.avax.network/ext/bc/C/rpc`
**ankr RPC:** `https://rpc.ankr.com/avalanche`
**localhost RPC:** `https://localhost:9650/ext/bc/C/rpc`

# BSC RPCs

**Type:** Mainnet
**Chain ID:** 56
**Symbol:** BNB
**Block explorer:** https://bscscan.com/

**binance RPC:** `https://bsc-dataseed.binance.org/`
**ankr RPC:** `https://bsc-dataseed.binance.org/`

# Ethereum RPCs

**Type:** Mainnet
**Chain ID:** 0x1
**Symbol:** ETH
**Block explorer:** https://etherscan.io/

**infura RPC:** `https://mainnet.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161`
**ankr RPC:** `https://rpc.ankr.com/eth`

# Polygon Matic RPCs

**Type:** Mainnet
**Chain ID:** 0x89
**Symbol:** MATIC
**Block explorer:** https://explorer.matic.network/

**polygon RPC:** `https://polygon-rpc.com/`

# Arbitrum RPCs

**Type:** Mainnet
**Chain ID:** 42161
**Symbol:** ETH
**Block explorer:** https://arbiscan.io/

**polygon RPC:** `https://rpc.ankr.com/arbitrum`

