# BIP85 Use Case

[BIP85](https://github.com/bitcoin/bips/blob/master/bip-0085.mediawiki) opens up the possibility of backing up only one seed, and use it to derive any future seeds.
The operation is also just a one-way operation, any derived seeds cannot be used to find out its parent seed.
Therefore, it simplifies backup processes a lot.

Our backup could, for example, only stores our seed phrase.
Then, for any new derived seeds, we can just store the derived seed's index.
A backup of our parent seed, is then effectively also a backup of all of our keys.

There has been some uses in crypto wallet technologies.
One of hardware wallet vendors that implement BIP85 is [coldcard](https://coldcard.com/docs/bip85), at least as far as I discovered.
One software wallet that does support deriving new seeds based on BIP85 that I know of is [Airgap Vault](https://support.airgap.it/features/bip85/).

In some cases, maybe you don't want to rely on coldcard firmware or Airgap Vault to derive your child seeds.
Therefore, you can also derive your seeds with the use of [Ian Coleman's Mnemonic Code Converter](https://iancoleman.io/bip39/).
Again, I am not sure how many times do I need to emphasize it, but:

**EXPOSING YOUR SEED PHRASE TO A NON-AIRGAPPED DEVICE MIGHT COMPROMISE YOUR SEED PHRASE!**[^nonairgapped]

*Airplane mode* is available to most modern devices.
Also, *incognito mode* that would delete all history and caches after the tab is closed is not hard to find in all good modern browsers, be it in mobile or desktop.

To use the Mnemonic Code Converter offline, you can either:

- load the page first in an incognito tab, then turn on airplane mode and use the tool;[^closetab] or

- download the latest version of the tool's [releases](https://github.com/iancoleman/bip39/releases), then copy it to an offline device[^offlinedev], and run the tool in an incognito mode.

[^nonairgapped]: Of course, this might be important for privacy and security conscious folks. Still, it is wise, and wouldn't hurt, to run the tool while offline only.
[^closetab]: Be sensible. Before turning off your airplane mode, don't forget to close the incognito tab first. Otherwise it would be the same as using the tool while online :)
[^offlinedev]: or just run it on the same device but offline and in an incognito tab.

However, it is worth noting that, unlike that advertised in the linked coldcard documentation, it is not true that all we need to back up our wallet is just our parent seed and its BIP85 derivation index.
According to the [BIP85 specification](https://github.com/bitcoin/bips/blob/master/bip-0085.mediawiki), the derived entropy is different according to its application number.
In case of deriving child BIP39 seeds, the derivation path format is: `m/83696968'/39'/{language}'/{words}'/{index}'`

We also discovered that there are at least three factors to consider: `language`, `words`, and `index`.
If you mostly use English-language wallets, then the language index would be `0'`.[^childlanguage]
For `words`, it refers to the number of words in your seed phrase, that could either be 12-words, 18-words, or 24-words.
Meanwhile, `index` part is trivial: it's just the derivation index of your mnemonics.

[^childlanguage]: I couldn't find any wallet in the market that uses other language for creating or importing seed phrases. Although it is highly unlikely, if you do use or have a seed that is not in english language, consult the BIP85 specification page for its language index, and your wallet's documentations.

For example, if you want to derive a 12-word seed phrase from your seed phrase with `index = 0`, the derivation path would be: `m/83696968'/39'/0'/12'/0'`.
For `index = 1`, the derivation path would be: `m/83696968'/39'/0'/12'/1'`.
Meanwhile, if you want to derive a 24-word seed phrase instead, they would be: `m/83696968'/39'/0'/24'/0'` and `m/83696968'/39'/0'/24'/1'` instead.

Therefore, assuming English-language seed phrases,[^followstandard] all you need to write down to back up all of your seed phrases is the following:

1. Your parent seed phrase. It encodes your original entropy, and without it, no entropy can be derived. Additionally, you also need your seed extension if you have one.[^bip85passphrase]

2. The size of your derived seed phrase. It could be a 12-words, 18-words, or 24-words seed phrase.

3. Your child seed phrase index.

[^followstandard]: Also optimistically assumes that your preferred wallet or tool follows BIP85 specification very closely.
[^bip85passphrase]: So far, I know only Ian Coleman's Mnemonic Code Converter and Airgap Vault implement this feature conveniently. Coldcard apparently allows deriving entropy from a passphrase protected seeds, but they discouraged it.

Assuming you just back up your seed phrases, without backing point 2 or point 3, there would be almost unlimited amount of combinations that you have to look out for.[^forwebtool]
In coldcard, even though the index runs just from `0000` to `9999` with marketedly, "there are only 10,000 possible choices", is not entirely correct.
Given that each derived seed can either be in a 12, 18, or 24 words format, the actual number of possible choices are `30,000`.[^insignificant]
For Airgap Vault, there can only be 30 possible choices though, as the app can only derive child seed phrases from index 0 to index 9.

[^forwebtool]: Ian Coleman's Mnemonic Code Converter tool seems to have no practical limit on index number. However, for curious folks, I've tested the tool's upper limit, which seems to be capable of deriving child mnemonics from index `0` to index `2147483647`, which means there are 2^31 possible index numbers. Though not impossible, finding the right random index number you use by trying one by one is impractical.
[^insignificant]: Although to be fair, it is still in the same order of magnitude as the originally quoted 10,000 possible choices. It will be a major inconvenience, but it is not impossible.

Given all of the above as our considerations, and that assuming only English seed phrases, also that every tool we are going to use implements BIP85 standard properly, we can conveniently record our derived seed phrases by storing these four values: `parent seed phrase`, `parent seed extension`, `words`, and `index`.

