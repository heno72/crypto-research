# ArDrive Permaweb Link

In this blog, I want to describe how we can have a permanent link to files, without worrying about possible disappearance of our uploaded files (server expiration, error, etc).
The inspiration is from ArDrive's page on [Using ArDrive with OpenSea](https://ardrive.io/using-ardrive-with-opensea/), and [Storing your NFT Data with ArDrive Web](https://ardrive.atlassian.net/wiki/spaces/help/pages/209879067/Storing+your+NFT+Data+with+ArDrive+Web)

![A demonstration of an Arweave hosted image](https://arweave.net/b48YzKE-WbVAGDOuQBDkWB8mTpWrovijxQualro5Pv8)

## A brief introduction

Arweave is a cryptocurrency with AR ticker, and is a utility token to pay for permaweb storage.
It is unique among other decentralized file storage solutions available in the crypto space, as it is designed in a way that any files uploaded there will last practically forever.
All uploaded files would then be practically immutable, and can't be deleted.

ArDrive seeks to provide some sort of mutability to the otherwise immutable storage solutions, by uploading "revisions" of existing uploads.
In a way it does not delete any previously uploaded versions, it only stores updated files separately, and the new metadata referring to the same file id is added.
In the same manner, we can "move" files and folders around by updating the metadata with its latest versions.

In the most simplest way to explain it, a file in an ArDrive Drive or Folder is comprised of at least two files: a metadata and the actual data.
Its metadata describes the file properties, and on which folder the file resides.

ArDrive allows the creation of a Public Drive and a Private Drive.
Like its name, a Private Drive can only be accessed with your wallet's private key, and symetrically encrypted with a password.
The design allows the drive's content to be secure post-quantum computers.
Asymmetric encryption is insecure against comercially viable quantum computers, as the use of [Shor's algorithm](https://en.wikipedia.org/wiki/Shor%27s_algorithm) becomes ridiculously cheap in a quantum computing system.
However, symmetric encryption like [AES-256 with a sufficiently large key size is expected to be sufficiently secure post-quantum.](https://blog.boot.dev/cryptography/is-aes-256-quantum-resistant/#:~:text=Symmetric%20encryption%2C%20or%20more%20specifically,key%20sizes%20are%20large%20enough)

However, the use of a private drive is not our focus in this article.
The use of public drive is our interest here, as it allows sharing of non-perishable static digital contents.

## Making a public drive

It is important to note that your drive should be a public drive, otherwise others would not be able to access your drive without your private key.
Once you acquire your [AR wallet](https://chrome.google.com/webstore/detail/arconnect/einnioafmpimabjcddiinlhmijaionap),[^arfaucet] and access [ArDrive's web frontend](https://app.ardrive.io/), you can now create a Public Drive.
The creation of a Public Drive costs some AR, as it is done by sending some metadatas (Drive and root folder metadatas are uploaded through the interface).

[^arfaucet]: Fun fact, you can actually get some free AR coin airdrop to test Arweave and ArDrive features from https://faucet.arweave.net/

