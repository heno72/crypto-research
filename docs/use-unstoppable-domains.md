# Unstoppable Domains

You can find a guide [here](https://medium.com/unstoppabledomains/gateways-to-the-decentralized-web-how-to-update-google-chrome-and-firefox-for-web3-34a497c8f33c).

Here's a recap for Chrome and Firefox:

1. Open Chrome settings, or type ```about:settings/security``` on the omnibox. Go to ```advanced``` and under ```Use secure DNS```, set it to ```With Custom```, and enter ```https://resolver.unstoppable.io/dns-query```
1. Open Mozilla Firefox settings, and proceed to ```Options > General Tab > Network Settings > Settings``` and set **Custom URL** as ```https://resolver.unstoppable.io/dns-query```

To start browsing, you either:

1. Type: ```http://domainname.crypto``` to the url box. You must use **http://** and not https://
1. Type: ```domainname.crypto/``` to the url box. The **forward slash** (```/```) is to prevent the browser from just searching through a search engine.

The DNS servers understand blockchain domains but browsers are yet to get it yet. Therefore, starting with ```http://``` or ending with ```/``` tells our browser to know that it's a website, not a search query.

Obviously, the ```domainname.crypto``` part is to be filled with whatever blockchain domain name you wish to access.

