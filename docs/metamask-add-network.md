# Adding Custom Networks on Metamask

Alternately, you can go [here](https://metamask.zendesk.com/hc/en-us/articles/360043227612-How-to-add-a-custom-network-RPC) for step by step guides on extension and mobile.
Also check [chainlist.wtf](https://chainlist.wtf/) and connect your metamask to add more networks.

## Adds BSC

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Binance Smart Chain` |
|New RPC URL | `https://bsc-dataseed.binance.org/` |
|ChainID | `56` |
|Symbol | `BNB` |
|Block Explorer URL | `https://bscscan.com` |
|Website | `https://www.binance.org/en` |

Source: [Metamask Zendesk, Network Profile: Binance Smart Chain/BSC (BNB)](https://metamask.zendesk.com/hc/en-us/articles/4415758120219-Network-profile-Binance-Smart-Chain-BSC-BNB-)

## Adds Polygon MATIC

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Polygon Mainnet` |
|New RPC URL | `https://polygon-rpc.com/` |
|ChainID | `137` |
|Symbol | `MATIC` |
|Block Explorer URL | `https://polygonscan.com/` |
|Website | `https://polygon.technology/` |

Source: [Metamask Zendesk, Network profile: Polygon (MATIC)](https://metamask.zendesk.com/hc/en-us/articles/4415758346267)

## Adds Arbitrum One

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Arbitrum One` |
|New RPC URL | `https://arb1.arbitrum.io/rpc` |
|ChainID | `42161` |
|Symbol | `ETH` |
|Block Explorer URL | `https://arbiscan.io/` |
|Website | `https://portal.arbitrum.one/` |

Source: [Metamask Zendesk, Network Profile: Arbitrum](https://metamask.zendesk.com/hc/en-us/articles/4415758358299-Network-profile-Arbitrum)

## Adds Optimism

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Optimism` |
|New RPC URL | `https://mainnet.optimism.io/` |
|ChainID | `10` |
|Symbol | `ETH` |
|Block Explorer URL | `https://optimistic.etherscan.io/` |
|Website | `https://www.optimism.io/` |

Source: [Metamask Zendesk, Network Profile: Optimism](https://metamask.zendesk.com/hc/en-us/articles/4415758352667-Network-profile-Optimism)

## Adds Gnosis Chain network (formerly xDai)

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Gnosis Chain` |
|New RPC URL | `https://rpc.gnosischain.com/` |
|ChainID | `0x64` |
|Symbol | `xDai` |
|Block Explorer URL | `https://blockscout.com/xdai/mainnet/` |
|Website | `https://developers.gnosischain.com/` |

Source: [Gnosis Chain - Metamask Setup](https://developers.gnosischain.com/for-users/wallets/metamask/metamask-setup)

## Adds Avalanche (AVAX)

Adds the following information:

| Field | Description |
| :---- | :---------- |
|Network Name | `Avalanche (C-Chain)` |
|New RPC URL | `https://api.avax.network/ext/bc/C/rpc` |
|ChainID | `43114` |
|Symbol | `AVAX` |
|Block Explorer URL | `https://snowtrace.io/` |
|Website | `https://avax.network/` |

Source: [Metamask Zendesk, Network Profile: Avalanche (AVAX)](https://metamask.zendesk.com/hc/en-us/articles/4415758179355-Network-profile-Avalanche-AVAX-)

